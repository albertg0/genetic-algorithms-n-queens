
#include "Genotype.h"
#include "Population.h"

using namespace std;

int main()
{
	int n = 10;
	
	ofstream file("output.txt", ios::app);
	Population p;

	srand(time(0));
	for (int i = 0; i < 25; i++)
	{
		p = Population(n);
		do
		{
			p.calculate_fitness();
			p.sort_population();
			p.create_mating_pool();
			p.crossover();
			p.inc_generation_count();
		} while (p.get_generation_counter() < 1000 && !p.solution_found());

		//p.print_population();
		
		cout << " Generation:: " << p.get_generation_counter() << "    ";
		p.get_fittest().print_genotype();
		cout << endl;
		
		file << i+1 << '\t';
		file << p.get_fittest().get_fitness_score() << '\t';
		file << p.get_generation_counter() << "\t\t";
		for (int i = 0; i < p.get_fittest().get_genotype_size(); i++)
		{
			file << i << p.get_fittest()[i] << '\t';
		}

		file << endl << endl;
	}

	p.~Population();
	file.close();
	return 0;
}

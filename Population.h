#ifndef POPULATION_H
#define POPULATION_H

#include <iostream>
#include <string>
#include <random>
#include<cstdlib>
#include <time.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <functional>
#include "Genotype.h"

class Population {

	Genotype * population;
	int population_size;
	std::vector<Genotype > parents;
	std::vector<Genotype > children;
	int generation_counter;

public:
	Population();
	Population(int n);
	~Population();
	/*	Create Mating Pool
	Tournament Selection
	*/
	void create_mating_pool();
	Genotype get_fittest();
	bool solution_found();
	void crossover();
	void inc_generation_count();
	int get_generation_counter();
	void print_parents();

	void calculate_fitness();

	void print_population();
	void sort_population();
	bool operator () (Genotype& o, Genotype& o1);
};


Population::Population() {
	population_size = 10;
};
Population::Population(int n) {

	population_size = 10;
	generation_counter = 0;

	if (population_size > 0) {
		population = new Genotype[population_size];

		for (int i = 0; i < population_size; i++) {
			population[i] = Genotype(n);
		}

	}
}
Population::~Population()
{
	parents.clear();
}
/*	Create Mating Pool
Tournament Selection
*/
void Population::create_mating_pool()
{
	int pool_size = population_size * 0.10;

	//Add an extra parent for odd mating pool sizes
	if (pool_size > 0 && !(pool_size % 2 == 0))
		pool_size++;

	int current_member = 1;

	const int ind_size = 3;
	Genotype individuals[ind_size];

	while (current_member <= pool_size)
	{
		for (int i = 0; i < 3; i++)
		{
			individuals[i] = population[rand() % population_size];
		}

		std::sort(individuals, individuals + ind_size, std::greater<Genotype>());
		parents.push_back(individuals[0]);
		current_member++;
	}


}

Genotype Population::get_fittest()
{
	return population[0];
}

bool Population::solution_found() {
	if (get_fittest().get_fitness_score() >= 1 / (0.0001))
	{
		std::cout << "Solition ::";
		return true;
	}
	else
		return false;
}
void Population::crossover()
{
	int g_size = parents[0].get_genotype_size();
	int c_pos1 = 0;

	while (!parents.empty())
	{
		c_pos1 = rand() % g_size;

		std::vector<Genotype> parent_pair;
		//Thinking this can get removed and made more direct in the next step
		if (!parents.empty())
		{
			parent_pair.push_back(parents.back());
			parents.pop_back();
		}
		if (!parents.empty())
		{
			parent_pair.push_back(parents.back());
			parents.pop_back();
		}

		//Copy parents into children, then overwrite at crossover points for each child
		const int child_pair_size = 2;
		Genotype  child_pair[child_pair_size];

		child_pair[0] = Genotype(g_size);
		child_pair[1] = Genotype(g_size);

		for (int i = 0; i < child_pair_size; i++)
		{
			for (int a = 0; a < g_size; a++)
			{
				child_pair[i].change_sequence_at(a, parent_pair[i][a]);
			}
		}
		int i;

		for (i = c_pos1; i < g_size; i++)
		{
			bool dup1 = false;
			bool dup2 = false;

			for (int a = 0; a <= i; a++)
			{
				if (child_pair[0][a] == parent_pair[0][i])
					dup1 = true;

				if (child_pair[1][a] == parent_pair[1][i])
					dup2 = true;
			}
			if (!dup1 && !dup2)
			{
				child_pair[0].change_sequence_at(i, parent_pair[1][i]);
				child_pair[1].change_sequence_at(i, parent_pair[0][i]);
			}

			dup1 = dup2 = false;
		}


		//*****************************MUTATION**************************************
		//Mutation processs
		//Takes a number from 0 - 99 to generate a 10% probability for mutation

		int mutationChild1 = rand() % 100;
		int mutationChild2 = rand() % 100;
		int m_pos1, m_pos2;
		int temp = 0;

		if (mutationChild1 < 10)
		{
			m_pos1 = rand() % 10;
			m_pos2 = rand() % 10;

			child_pair[0].mutationSwap(m_pos1, m_pos2);

		}
		if (mutationChild2 < 10)
		{
			m_pos1 = rand() % 10;
			m_pos2 = rand() % 10;

			child_pair[1].mutationSwap(m_pos1, m_pos2);

		}

		children.push_back(child_pair[0]);
		children.push_back(child_pair[1]);

	}


	for (int a = 0; a < children.size(); a++)
	{
		children[a].fitness_calculator();
	}

	int c_size = children.size();
	Genotype * new_pop = new Genotype[population_size + children.size()];

	for (int i = 0; i < population_size; i++)
	{
		for (int a = 0; a < g_size; a++)
		{
			new_pop[i] = population[i];
		}

	}

	int j = population_size;
	while (!children.empty())
	{
		new_pop[j] = children.back();
		children.pop_back();
		j++;

	}

	std::sort(new_pop, new_pop + (population_size + c_size), std::greater<Genotype>());

	for (int x = 0; x < population_size; x++)
	{
		population[x] = new_pop[x];

	}


	delete[] new_pop;
	children.clear();


}
void Population::inc_generation_count() {
	generation_counter++;
}
int Population::get_generation_counter()
{
	return generation_counter;
}
void Population::print_parents()
{
	for (int i = 0; i < parents.size(); i++)
	{
		std::cout << parents[i].get_fitness_score() << std::endl;
	}
}

void Population::calculate_fitness()
{
	for (int i = 0; i < population_size; i++) {
		population[i].fitness_calculator();

	}
}

void Population::print_population() {
	for (int i = 0; i < population_size; i++) {
		population[i].print_genotype();
		std::cout << '\n';
	}
}

void Population::sort_population() {

	std::sort(population, population + population_size, std::greater<Genotype>());

}
bool Population::operator () (Genotype& o, Genotype& o1) {
	return (o.get_fitness_score() < o1.get_fitness_score());
}
#endif // !POPULATION_H

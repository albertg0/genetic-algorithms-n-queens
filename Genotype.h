#ifndef GENOTYPE_H
#define GENOTYPE_H

#include <iostream>
#include <iostream>
#include <string>
#include <random>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <functional>



class Genotype {

	int genotype_size;
	int *genotype;
	double fitness_score;

public:

	Genotype();
	Genotype(int n);
	Genotype(const Genotype& obj);

	double get_fitness_score();
	int get_genotype_size();
	void print_genotype();
	void set_fitness(int n);
	void fitness_calculator();

	//Overloaded operators for  STL sort algorithm
	inline bool operator>  (const Genotype& o) const;
	inline  bool operator< (const Genotype& o) const;
	inline int operator[] (const int i) const;

	void change_sequence_at(int i, int s);
	void mutationSwap(int i, int j);
};

Genotype::Genotype() {
	genotype_size = 10;
	fitness_score = 0.0;
};

Genotype::Genotype(int n) {

	genotype_size = n;
	fitness_score = 0.0;
	if (n > 0) {
		genotype = new int[genotype_size];
		for (int i = 0; i < genotype_size; i++) {
			genotype[i] = i;
		}

		for (int i = 0; i < genotype_size; i++) {
			std::swap(genotype[i], genotype[rand() % genotype_size]);
		}


	}

}

Genotype::Genotype(const Genotype& obj)
{
	genotype_size = obj.genotype_size;
	genotype = new int[genotype_size];

	for (int i = 0; i < genotype_size; i++)
	{
		genotype[i] = obj[i];
	}
	fitness_score = obj.fitness_score;
}
double Genotype::get_fitness_score()
{
	return fitness_score;
}
int Genotype:: get_genotype_size() {
	return genotype_size;
}
void Genotype::print_genotype() {
	for (int i = 0; i < genotype_size; i++) {
		std::cout << i << genotype[i] << " ";
	}
	std::cout << "  Fitness Score :: " << fitness_score;
}
void Genotype::set_fitness(int n) {
	fitness_score = double(1.0 / (n + 0.0001));
}

void Genotype::fitness_calculator() {

	int conflict_counter = 0;
	if (genotype)
	{
		//diagonal down + left
		////Check each value in the genotype
		for (int i = 0; i < genotype_size; i++)
		{

			int c = 1;
			//For each value, start checking in the next position diagonally adjacent.
			//b = i+1 is the next column, c is added to the value to offset it to the diagonal adjacent position
			//genotype[i]+c is equivalent to j+1. For example, if the initial values we're checking are
			// i = 0 and j = 1, the adjacent square is accessed as i+1 and j+1. i = 1 and j = 2.
			//The conditional statement in the for loop accounts for out of bounds errors.
			for (int b = i + 1; genotype[i] + c < genotype_size && b < genotype_size; b++)
			{

				if (genotype[i] + c <= genotype_size && genotype[i] + c == genotype[b])
				{
					conflict_counter++;
					break;
				}
				c++;
			}
			c = 0;
		}

		//Diagonal right + up
		for (int i = 0; i < genotype_size; i++)
		{

			int c2 = 1;
			for (int b = i + 1; genotype[i] - c2 >= 0 && b < genotype_size; b++)
			{

				if (genotype[i] - c2 >= 0 && genotype[i] - c2 == genotype[b])
				{
					conflict_counter++;
					break;
				}
				c2++;
			}
			c2 = 0;
		}

		// left up
		for (int i = genotype_size - 1; i > 0; i--)
		{
			int c2 = 1;
			for (int b = i - 1; genotype[i] - c2 > 0 && b > 0; b--)
			{
				if (genotype[i] - c2 > 0 && genotype[i] - c2 == genotype[b])
				{
					conflict_counter++;
					break;
				}
				c2++;
			}
			c2 = 0;
		}

		//down + right
		for (int i = genotype_size - 1; i > 0; i--)
		{
			int c2 = 1;
			for (int b = i - 1; genotype[i] + c2 < genotype_size && b > 0; b--)
			{
				if (genotype[i] + c2 < genotype_size && genotype[i] + c2 == genotype[b])
				{
					conflict_counter++;
					break;
				}
				c2++;
			}
			c2 = 0;
		}

		set_fitness(conflict_counter);
	}
}
//Overloaded operators for  STL sort algorithm
inline bool Genotype:: operator>  (const Genotype& o) const {
	return (fitness_score > o.fitness_score);
}
inline  bool Genotype::operator< (const Genotype& o) const {
	return (fitness_score < o.fitness_score);
}
inline int Genotype::operator[] (const int i) const {
	if (i >= 0 && i < genotype_size)
		return genotype[i];
	else
		std::cout << "Out of bounds error::operator[]";
}
void Genotype::change_sequence_at(int i, int s)
{
	if (i >= 0 && i < genotype_size)
		genotype[i] = s;
	else
		std::cout << "Out of bounds error::change sequence ";
}
void Genotype::mutationSwap(int i, int j)
{
	if (i >= 0 && i < genotype_size)
	{

		int temp = genotype[i];
		genotype[i] = genotype[j];
		genotype[j] = temp;
	}

}


#endif // !1
